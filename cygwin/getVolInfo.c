/* getvolinfo.c
 *   Originally published:
 *      http://cygwin.com/ml/cygwin/2007-08/msg00040.html
 *
 * Copyright (c) 2008, Corinna Vinschen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <sys/cygwin.h>
#define _WIN32_WINNT 0x0a00
#include <windows.h>
#include <winternl.h>
#include <ntstatus.h>

/* winternl.h is seriously lacking */
NTSTATUS NTAPI RtlDosPathNameToNtPathName_U_WithStatus (PCWSTR,
							PUNICODE_STRING,
							PCWSTR *,
							VOID *);

#define FILE_REMOVABLE_MEDIA 0x00000001
#define FILE_REMOTE_DEVICE 0x00000010

#define FileFsSectorSizeInformation (FileFsVolumeFlagsInformation + 1)

typedef struct _FILE_FS_SECTOR_SIZE_INFORMATION
{
  ULONG LogicalBytesPerSector;
  ULONG PhysicalBytesPerSectorForAtomicity;
  ULONG PhysicalBytesPerSectorForPerformance;
  ULONG FileSystemEffectivePhysicalBytesPerSectorForAtomicity;
  ULONG Flags;
  ULONG ByteOffsetForSectorAlignment;
  ULONG ByteOffsetForPartitionAlignment;
} FILE_FS_SECTOR_SIZE_INFORMATION, *PFILE_FS_SECTOR_SIZE_INFORMATION;

/* Sector Size Information Flags */
#define SSINFO_FLAGS_ALIGNED_DEVICE              0x00000001
#define SSINFO_FLAGS_PARTITION_ALIGNED_ON_DEVICE 0x00000002
#define SSINFO_FLAGS_NO_SEEK_PENALTY             0x00000004
#define SSINFO_FLAGS_TRIM_ENABLED                0x00000008
#define SSINFO_FLAGS_BYTE_ADDRESSABLE            0x00000010

void
_print_flag (ULONG flags, ULONG val, const char *name)
{
  printf ("  %-34s: %s\n", name, (flags & val) ? "TRUE" : "FALSE");
}

#define print_flag(_flags,_val)	_print_flag(_flags, _val, #_val)

int
main (int argc, char **argv)
{
  OBJECT_ATTRIBUTES attr;
  UNICODE_STRING upath;
  IO_STATUS_BLOCK io;
  NTSTATUS stat;
  HANDLE h;

  BOOL remote = FALSE;
  wchar_t *winpath;
  char buf[4096];

  setlocale (LC_ALL, "");

  if (argc < 2)
    {
      fprintf (stderr, "usage: %s path\n", argv[0]);
      return 1;
    }

  winpath = (wchar_t *) cygwin_create_path (CCP_POSIX_TO_WIN_W | CCP_ABSOLUTE,
					    argv[1]);
  if (winpath == NULL)
    {
      fprintf (stderr, "create_cygwin_path (posix to win32 absolute) failed\n");
      return 1;
    }

  stat = RtlDosPathNameToNtPathName_U_WithStatus (winpath, &upath, NULL, NULL);
  if (!NT_SUCCESS (stat))
    {
      fprintf (stderr, "RtlDosPathNameToNtPathName_U_WithStatus(%s) failed, "
		       "0x%08x\n", winpath, stat);
      free (winpath);
      return 1;
    }

  InitializeObjectAttributes (&attr, &upath, OBJ_CASE_INSENSITIVE, NULL, NULL);
  stat = NtOpenFile (&h, READ_CONTROL, &attr, &io, FILE_SHARE_VALID_FLAGS,
		     FILE_OPEN_FOR_BACKUP_INTENT);
  if (stat == STATUS_INVALID_PARAMETER)
    stat = NtOpenFile (&h, FILE_READ_DATA, &attr, &io,
			 FILE_SHARE_VALID_FLAGS, FILE_OPEN_FOR_BACKUP_INTENT);
  if (stat == STATUS_NO_MEDIA_IN_DEVICE)
    {
      upath.Length = 6 * sizeof (WCHAR);
      stat = NtOpenFile (&h, READ_CONTROL, &attr, &io,
			 FILE_SHARE_VALID_FLAGS, 0);
    }
  if (!NT_SUCCESS (stat))
    {
      fprintf (stderr, "NtOpenFile(%.*ls) failed, 0x%08x\n",
		       upath.Length / sizeof (WCHAR), upath.Buffer,
		       stat);
      return 1;
    }
  /* FileFsDeviceInformation */
  stat = NtQueryVolumeInformationFile (h, &io, buf, sizeof buf,
				       FileFsDeviceInformation);
  if (NT_SUCCESS (stat))
    {
      PFILE_FS_DEVICE_INFORMATION pfi =
	    (PFILE_FS_DEVICE_INFORMATION) buf;
      printf ("Device Type        : 0x%02x\n", pfi->DeviceType);
      printf ("Characteristics    : 0x%08x\n", pfi->Characteristics);
      print_flag (pfi->Characteristics, FILE_REMOVABLE_MEDIA);
      print_flag (pfi->Characteristics, FILE_REMOTE_DEVICE);

      remote = !!(pfi->Characteristics & FILE_REMOTE_DEVICE);
    }
  else
    fprintf (stderr, "FileFsDeviceInformation failed, 0x%08x\n", stat);
  /* FileFsVolumeInformation */
  stat = NtQueryVolumeInformationFile (h, &io, buf, sizeof buf,
				       FileFsVolumeInformation);
  if (NT_SUCCESS (stat))
    {
      PFILE_FS_VOLUME_INFORMATION pfi =
	    (PFILE_FS_VOLUME_INFORMATION) buf;
      if (pfi->VolumeLabelLength)
	{
	  printf ("Volume Name        : <%.*ls>\n",
		  pfi->VolumeLabelLength / sizeof (WCHAR), pfi->VolumeLabel);
	}
      else
	printf ("Volume Name        : <>\n");
      printf ("Serial Number      : %u\n", pfi->VolumeSerialNumber);
    }
  else
    fprintf (stderr, "FileFsVolumeInformation failed, 0x%08x\n", stat);
  /* FileFsAttributeInformation */
  stat = NtQueryVolumeInformationFile (h, &io, buf, sizeof buf,
				       FileFsAttributeInformation);
  if (NT_SUCCESS (stat))
    {
      PFILE_FS_ATTRIBUTE_INFORMATION pfi =
	    (PFILE_FS_ATTRIBUTE_INFORMATION) buf;
      printf ("Max Filenamelength : %u\n",pfi->MaximumComponentNameLength);
      printf ("Filesystemname     : <%.*ls>\n",
	      pfi->FileSystemNameLength / sizeof (WCHAR), pfi->FileSystemName);
      printf ("Flags              : 0x%08x\n", pfi->FileSystemAttributes);
      print_flag (pfi->FileSystemAttributes, FILE_CASE_SENSITIVE_SEARCH);
      print_flag (pfi->FileSystemAttributes, FILE_CASE_PRESERVED_NAMES);
      print_flag (pfi->FileSystemAttributes, FILE_UNICODE_ON_DISK);
      print_flag (pfi->FileSystemAttributes, FILE_PERSISTENT_ACLS);
      print_flag (pfi->FileSystemAttributes, FILE_FILE_COMPRESSION);
      print_flag (pfi->FileSystemAttributes, FILE_VOLUME_QUOTAS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_SPARSE_FILES);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_REPARSE_POINTS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_REMOTE_STORAGE);
      print_flag (pfi->FileSystemAttributes, FILE_RETURNS_CLEANUP_RESULT_INFO);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_POSIX_UNLINK_RENAME);
      print_flag (pfi->FileSystemAttributes, FILE_VOLUME_IS_COMPRESSED);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_OBJECT_IDS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_ENCRYPTION);
      print_flag (pfi->FileSystemAttributes, FILE_NAMED_STREAMS);
      print_flag (pfi->FileSystemAttributes, FILE_READ_ONLY_VOLUME);
      print_flag (pfi->FileSystemAttributes, FILE_SEQUENTIAL_WRITE_ONCE);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_TRANSACTIONS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_HARD_LINKS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_EXTENDED_ATTRIBUTES);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_OPEN_BY_FILE_ID);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_USN_JOURNAL);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_INTEGRITY_STREAMS);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_BLOCK_REFCOUNTING);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_SPARSE_VDL);
      print_flag (pfi->FileSystemAttributes, FILE_DAX_VOLUME);
      print_flag (pfi->FileSystemAttributes, FILE_SUPPORTS_GHOSTING);
    }
  else
    fprintf (stderr, "FileFsAttributeInformation failed, 0x%08x\n", stat);
  /* FileFsSectorSizeInformation */
  stat = NtQueryVolumeInformationFile (h, &io, buf, sizeof buf,
				       FileFsSectorSizeInformation);
  if (NT_SUCCESS (stat))
    {
      PFILE_FS_SECTOR_SIZE_INFORMATION pfi =
	    (PFILE_FS_SECTOR_SIZE_INFORMATION) buf;
      printf ("SectorInfoFlags    : 0x%02x\n", pfi->Flags);
      print_flag (pfi->Flags, SSINFO_FLAGS_NO_SEEK_PENALTY);
      print_flag (pfi->Flags, SSINFO_FLAGS_TRIM_ENABLED);
    }
  /* Do not generate an error message if the FS is remote. */
  else if (!remote)
    fprintf (stderr, "FileFsSectorSizeInformation failed, 0x%08x\n", stat);

  NtClose (h);
  free (winpath);
  RtlFreeUnicodeString (&upath);
  return 0;
}

